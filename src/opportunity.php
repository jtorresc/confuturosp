<?php

    # Omitir limite de tiempo, memoria y errores
    ini_set('memory_limit', '6400000000000');
    set_time_limit(0);
    error_reporting(0);
    
    # Librerias
    require '../vendor/autoload.php';
    require('../src/Coseva/csv.php');
    require('../config/connBD.php');

    # Lectura .env
    $dotenv = Dotenv\Dotenv::createImmutable('../');
    $dotenv->load();

    # Nombre de la tabla
    $tablename = $_ENV['TABLENAME_OPPORTUNITY'];

    # Limpieza de bd
    $queryTruncate = "TRUNCATE TABLE dbo." . $tablename;
    $truncateTable = sqlsrv_query($conn, $queryTruncate);
    if($truncateTable === false){
        die(print_r(sqlsrv_errors(), true));
    }

    # Busqueda de csv en directorio
    $path = '../files/';
    $file = scandir($path);
    $wordOppOpen = 'abiertas';
    $wordOppFiled = 'archivadas';
    $wordOppWon = 'ganadas';
    $wordOppLost = 'perdidas';
    $pathFileOpen = '';
    $pathFileFiled = '';
    $pathFileWon = '';
    $pathFileLost = '';
    foreach($file as $data){
        if(preg_match("/$wordOppOpen/i", $data)) $pathFileOpen = $data;
        $filePathOpen = $path . $pathFileOpen;
        if(preg_match("/$wordOppFiled/i", $data)) $pathFileFiled = $data;
        $filePathFiled = $path . $pathFileFiled;
        if(preg_match("/$wordOppWon/i", $data)) $pathFileWon = $data;
        $filePathWon = $path . $pathFileWon;
        if(preg_match("/$wordOppLost/i", $data)) $pathFileLost = $data;
        $filePathLost = $path . $pathFileLost;
    }

    # Creacion de array segun estado oportunidad
    $path = [
        'PERDIDAS' => $filePathLost,
        'GANADAS' => $filePathWon,
        'ARCHIVADAS' => $filePathFiled,
        'ABIERTAS' => $filePathOpen
    ]; 

    # Variables en proceso
    $arrayLead = [];
    $i = 0;

    # LLenado y limpieza de array
    foreach($path as $key => $value){
        $csv = new Coseva\CSV($value);
        $csv->parse();
        foreach($csv as $keyCsv => $valueCsv){
            $arrayLead[$i][$keyCsv] = str_replace("'", " ",$valueCsv);
            array_push($arrayLead[$i][$keyCsv], $key);
            unset($arrayLead[$i][0]);
        }
        $i++;
    }

    # Union de arrays en uno y Eliminar valores vacios de array
    $arrayLead = array_merge($arrayLead[0], $arrayLead[1], $arrayLead[2], $arrayLead[3]);
    foreach($arrayLead as $key => $value){
        if(empty($value[0])){
            unset($arrayLead[$key]);
        }
    }

    # Proceso de llenado y envio
    $arrayLead = array_chunk($arrayLead, 999);
    foreach($arrayLead as $lead){
        foreach($lead as $data){
            if(isset($data[0])){

                # Variables a enviar
                $oppID = $data[0];
                $oppName = $data[1];
                $dealStageID = $data[2];
                $dealStageName = $data[3];
                $ownerID = $data[4];
                $ownerName = $data[5];
                $ownerEmail = $data[6];
                $originatindLeadID = $data[7];
                $originatindLeadName = $data[8];
                $originatindLeadEmail = $data[9];
                $primaryLeadID = $data[10];
                $primaryLeadName = $data[11];
                $primaryLeadEmail = $data[12];
                $companyName = $data[13];
                $accountName = $data[14];
                $campaignName = $data[15];
                $amount = $data[16];
                $probability = $data[17];
                $description = $data[18];
                $expectedValue = $data[19];
                $isWon = $data[20];
                $isClosed = $data[21];
                $closeDate = $data[22];
                $lastActivityDate = $data[23];
                $dateCreated = $data[24];
                $calls = $data[25];
                $emails = $data[26];
                $followUps = $data[27];
                $crmOppID = $data[28];
                $adjuntarAceptacionOferta = $data[29];
                $tipoPension = $data[30];
                $porQueDesistioContratar = $data[31];
                $casoIrseOtraCompania = $data[32];
                $etapaInicialProceso = $data[33];
                $medioCualConoce = $data[34];
                $nScomp = $data[35];
                $motivoCierreOtraCompania = $data[36];
                $motivoCierreConfuturo = $data[37];
                $rut = $data[38];
                $referidoConsultor = $data[39];
                $director = $data[40];
                $estadoOportunidad = $data[41];
                $params[] = "(
                    '$oppID',
                    '$oppName',
                    '$dealStageID',
                    '$dealStageName',
                    '$ownerID',
                    '$ownerName',
                    '$ownerEmail',
                    '$originatindLeadID',
                    '$originatindLeadName',
                    '$originatindLeadEmail',
                    '$primaryLeadID',
                    '$primaryLeadName',
                    '$primaryLeadEmail',
                    '$companyName',
                    '$accountName',
                    '$campaignName',
                    '$amount',
                    '$probability',
                    '$description',
                    '$expectedValue',
                    '$isWon',
                    '$isClosed',
                    '$closeDate',
                    '$lastActivityDate',
                    '$dateCreated',
                    '$calls',
                    '$emails',
                    '$followUps',
                    '$crmOppID',
                    '$adjuntarAceptacionOferta',
                    '$tipoPension',
                    '$porQueDesistioContratar',
                    '$casoIrseOtraCompania',
                    '$etapaInicialProceso',
                    '$medioCualConoce',
                    '$nScomp',
                    '$motivoCierreOtraCompania',
                    '$motivoCierreConfuturo',
                    '$rut',
                    '$referidoConsultor',
                    '$director',
                    '$estadoOportunidad'
                )";
            }
        }

        # Query sql
        $queryInsert = "INSERT INTO dbo.".$tablename."
            (oppID, 
            oppName, 
            dealStageID,
            dealStageName,
            ownerID,
            ownerName,
            ownerEmail,
            originatingLeadID,
            originatingLeadName,
            originatingEmail,
            primaryLeadID,
            primaryLeadName,
            primaryLeadEmail,
            companyName,
            accountName,
            campaignName,
            amount,
            probability,
            descriptionOpp,
            expectedValue,
            isWon,
            isClosed,
            closeDate,
            lasActivityDate,
            dateCreated,
            calls,
            emails,
            followUps,
            crmOppID,
            adjuntarAceptacionOferta,
            tipoPension,
            porQueDesistioContratar,
            casoIrseOtraCompania,
            etapaInicialProceso,
            medioCualconoce,
            nScomp,
            motivoCierreCompania,
            motivoCierreConfuturo,
            rut,
            referidoConsultor,
            director,
            estadoOportunidad)
        VALUES " . implode(', ', $params);

        # Envio a BD
        $send = sqlsrv_query($conn, $queryInsert);
        if($send === false){
            die(print_r(sqlsrv_errors(), true));
        }
        
        # Reinicio de variable
        unset($params);
        
    }