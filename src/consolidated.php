<?php

    # Omitir limite de tiempo, memoria y errores
    ini_set('memory_limit', '6400000000000');
    set_time_limit(0);
    error_reporting(0);
    
    # Librerias
    require '../vendor/autoload.php';
    require('../src/Coseva/csv.php');
    require('../config/connBD.php');

    # Lectura .env
    $dotenv = Dotenv\Dotenv::createImmutable('../');
    $dotenv->load();

    # Nombre de la tabla
    $tablename = $_ENV['TABLENAME_CONSOLIDATED'];

    # Limpieza de bd
    $queryTruncate = "TRUNCATE TABLE dbo." . $tablename;
    $truncateTable = sqlsrv_query($conn, $queryTruncate);
    if($truncateTable === false){
        die(print_r(sqlsrv_errors(), true));
    }

    # Busqueda de csv en directorio
    $path = '../files/';
    $file = scandir($path);
    $word = 'leads';
    $pathFile = '';
    foreach($file as $data){
        if(preg_match("/$word/i", $data)) $pathFile = $data;
        $filePath = $path . $pathFile;
    }

    # Lectura de csv
    $pathFile = $filePath;
    $csv = new Coseva\CSV($pathFile);
    $csv->parse();
    $arrayLead = [];

    # Creacion de array y limpieza 
    foreach($csv as $key => $value){
        $arrayLead[$key] = str_replace("'", " ",$value);
        unset($arrayLead[0]);
    }

    # Eliminar valores vacios de array
    foreach($arrayLead as $key => $value){
        if(empty($value[0])){
            unset($arrayLead[$key]);
        }
    }

    # Proceso de llenado y envio
    $arrayNoDuplicated = deleteDuplicate($arrayLead, '15');
    $arrayLead = array_chunk($arrayLead, 999);
    foreach($arrayLead as $lead){
        foreach($lead as $value){
            if(isset($value[0])){

                 # Variables a enviar
                $leadCreateDate = $value[0];
                $leadOwner = $value[1];
                $campaignName = $value[2];
                $adGroupName = $value[3];
                $keyword = $value[4];
                $accountName = $value[5];
                $emailEngagementScore = $value[6];
                $primaryCampaignID = $value[7];
                $primaryCampaignName = $value[8];
                $unsubscribeReason = $value[9];
                $unsubscribeReasonOther = $value[10];
                $leadScoreWithDecay = $value[12];
                $optIP = $value[12];
                $optTimestamp = $value[13];
                $optMethod = $value[14];
                $sharpspringID = $value[15];
                $externalCrmID = $value[16];
                $firstName = $value[17];
                $lastName = $value[18];
                $email = $value[19];
                $companyName = $value[20];
                $title = $value[21];
                $industry = $value[22];
                $website = $value[23];
                $phoneNumber = $value[24];
                $extension = $value[25];
                $officePhoneNumber = $value[26];
                $numberBounces = $value[27];
                $hardBouncedEmailAddress = $value[28];
                $mobilePhone = $value[29];
                $fax = $value[30];
                $street = $value[31];
                $city = $value[32];
                $country = $value[33];
                $state = $value[34];
                $zip = $value[35];
                $description = $value[36];
                $leadScore = $value[37];
                $hasOpportunity = $value[38];
                $isQualified = $value[39];
                $isContact = $value[40];
                $isCustomer = $value[41];
                $leadStatus = $value[42];
                $isUnsubscribed = $value[43];
                $ownerID = $value[44];
                $iWouldDescribeMyself = $value[45];
                $gdprConsent = $value[46];
                $facebookLeadAds = $value[47];
                $rut = $value[48];
                $fechaNacimiento = $value[49];
                $tipoPension = $value[50];
                $realizaEstudioAsesor = $value[51];
                $edad = $value[52];
                $producto = $value[53];
                $modeloIngresoScomp = $value[54];
                $sexo = $value[55];
                $fechaPosibleIngresoScomp = $value[56];
                $respaldoAsesorFuturoPensiones = $value[57];
                $valorPrimaUF = $value[58];
                $afp = $value[59];
                $estadoGestion = $value[60];
                $subEstado = $value[61];
                $fechaCambioEstado = $value[62];
                $seleccionesCompania = $value[63];
                $genesys = $value[64];
                $completaFormulario1vez = $value[65];
                $listaPublico = $value[66];
                $consultor = $value[67];

                $params[] = "(
                    '$leadCreateDate',
                    '$leadOwner',
                    '$campaignName',
                    '$adGroupName',
                    '$keyword',
                    '$accountName',
                    '$emailEngagementScore',
                    '$primaryCampaignID',
                    '$primaryCampaignName',
                    '$unsubscribeReason',
                    '$unsubscribeReasonOther',
                    '$leadScoreWithDecay',
                    '$optIP',
                    '$optTimestamp',
                    '$optMethod',
                    '$sharpspringID',
                    '$externalCrmID',
                    '$firstName',
                    '$lastName',
                    '$email',
                    '$companyName',
                    '$title',
                    '$industry',
                    '$website',
                    '$phoneNumber',
                    '$extension',
                    '$officePhoneNumber',
                    '$numberBounces',
                    '$hardBouncedEmailAddress',
                    '$mobilePhone',
                    '$fax',
                    '$street',
                    '$city',
                    '$country',
                    '$state',
                    '$zip',
                    '$description',
                    '$leadScore',
                    '$hasOpportunity',
                    '$isQualified',
                    '$isContact',
                    '$isCustomer',
                    '$leadStatus',
                    '$isUnsubscribed',
                    '$ownerID',
                    '$iWouldDescribeMyself',
                    '$gdprConsent',
                    '$facebookLeadAds',
                    '$rut',
                    '$fechaNacimiento',
                    '$tipoPension',
                    '$realizaEstudioAsesor',
                    '$edad',
                    '$producto',
                    '$modeloIngresoScomp',
                    '$sexo',
                    '$fechaPosibleIngresoScomp',
                    '$respaldoAsesorFuturoPensiones',
                    '$valorPrimaUF',
                    '$afp',
                    '$estadoGestion',
                    '$subEstado',
                    '$fechaCambioEstado',
                    '$seleccionesCompania',
                    '$genesys',
                    '$completaFormulario1vez',
                    '$listaPublico',
                    '$consultor'
                )";
            }
        }

        # Limpiar array y pasar todo a string
        $params = array_map('stripslashes', $params);

        # Query sql
        $queryInsert = "INSERT INTO dbo.".$tablename."
            (leadCreateDate,
            leadOwner,
            campaignName,
            adGroupName,
            keyword,
            accountName,
            emailEngagementScore,
            primaryCampaignID,
            primaryCampaignName,
            unsubscribeReason,
            unsubscribeReasonOther,
            leadScoreWithDecay,
            optIP,
            optTimestamp,
            optMethod,
            sharpspringID,
            externalCrmID,
            firstName,
            lastName,
            email,
            companyName,
            title,
            industry,
            website,
            phoneNumber,
            extension,
            officePhoneNumber,
            numberBounces,
            hardBouncedEmailAddress,
            mobilePhone,
            fax,
            street,
            city,
            country,
            state,
            zip,
            description,
            leadScore,
            hasOpportunity,
            isQualified,
            isContact,
            isCustomer,
            leadStatus,
            isUnsubscribed,
            ownerID,
            iWouldDescribeMyself,
            gdprConsent,
            facebookLeadAds,
            rut,
            fechaNacimiento,
            tipoPension,
            realizaEstudioAsesorFuturo,
            edad,
            producto,
            modeloIngresoScomp,
            sexo,
            fechaPosibleIngresoScomp,
            respaldoAsesorFuturoPensiones,
            valorPrimaUF,
            afp,
            estadoGestion,
            subEstado,
            fechaCambioEstado,
            seleccionesCompania,
            genesys,
            completaFormulario1vez,
            listadoPublico,
            consultor)
        VALUES " . implode(', ', $params);

        # Envio a BD
        $send = sqlsrv_query($conn, $queryInsert);
        if($send === false){
            die(print_r(sqlsrv_errors(), true));
        }

        # Reinicio de variable
        unset($params);

    }

    # Funcion para eliminar valores duplicados
    function deleteDuplicate($array, $keyname){
        $newArray = [];  
        foreach($array as $key=>$value){
            if(isset($value[$keyname])){
                if(!isset($newArray[$value[$keyname]])){
                    $newArray[$value[$keyname]] = $value;
                }
            }
        }
        $newArray = array_values($newArray);
        return $newArray;
    }