<?php

    # Librerias
    require '../vendor/autoload.php';

    # Lectura .env
    $dotenv = Dotenv\Dotenv::createImmutable('../');
    $dotenv->load();

    # Conexion sql server
    $server = $_ENV['SQL_SERVER'];
    $data = array(
        "Database" => $_ENV['SQL_DATABASE'],
        "UID" => $_ENV['SQL_USER'],
        "PWD" => $_ENV['SQL_PASSWORD'],
        "CharacterSet" => "UTF-8"
    );
    $conn = sqlsrv_connect($server, $data);
    if($conn === false){
        die(print_r(sqlsrv_errors(), true));
    }